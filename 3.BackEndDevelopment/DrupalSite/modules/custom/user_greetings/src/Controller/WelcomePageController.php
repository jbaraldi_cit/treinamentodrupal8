<?php
/**
 * Created by PhpStorm.
 * User: jbaraldi
 * Date: 06/08/18
 * Time: 13:59
 */

namespace Drupal\user_greetings\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\user_greetings\Service\WelcomeMessageGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class WelcomePageController extends ControllerBase
{
    private $welcomeMessageGenerator;

    public function __construct($welcomeMessageGenerator)
    {
        $this->welcomeMessageGenerator = $welcomeMessageGenerator;
    }

    public function welcome($username)
    {
        return new Response($this->welcomeMessageGenerator->getWelcomeMessage($username));
    }

    public static function create(ContainerInterface $container)
    {
        return new static($container->get('user_greetings.welcome_message_generator'));
    }
}