<?php
/**
 * Created by PhpStorm.
 * User: jbaraldi
 * Date: 15/08/18
 * Time: 11:06
 */

namespace Drupal\user_greetings\Service;


class WelcomeMessageGenerator
{
    public function getWelcomeMessage($username)
    {
        if (!is_numeric($username)) {
            return 'Welcome, ' . $username . ', to my new Drupal 8 website!';
        }
        else {
            return 'Welcome to my new Drupal 8 website!';
        }
    }
}