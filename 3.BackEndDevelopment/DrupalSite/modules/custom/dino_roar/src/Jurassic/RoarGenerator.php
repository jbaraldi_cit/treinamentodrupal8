<?php
/**
 * Created by PhpStorm.
 * User: jbaraldi
 * Date: 07/08/18
 * Time: 09:01
 */

namespace Drupal\dino_roar\Jurassic;


use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

class RoarGenerator
{
    /**
     * @var KeyValueFactoryInterface
     */
    private $keyValueFactory;
    private $useCache;

    public function __construct(KeyValueFactoryInterface $keyValueFactory, $useCache)
    {
        $this->keyValueFactory = $keyValueFactory;
        $this->useCache = $useCache;
    }

    public function getRoar($length)
    {
        $key = 'roar_'.$length;
        $store = $this->keyValueFactory->get('dino');

        if ($this->useCache && $store->has($key)){
            return $store->get($key);
        }

        sleep(5);

        $str = 'RRR'.str_repeat('OO', $length).'ARR!!!';
        if ($this->useCache) {
            $store->set($key, $str);
        }

        return $str;
    }
}