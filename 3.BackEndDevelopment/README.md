#EXERCISES

---

1.) Building a module
---
[PSR-4 namespaces and autoloading in Drupal 8](https://www.drupal.org/node/2156625)
[Creating custom modules - Basic structure](https://www.drupal.org/docs/8/creating-custom-modules/basic-structure)
[Let D8 know about your module with an .info.yml file](https://www.drupal.org/docs/8/creating-custom-modules/let-drupal-8-know-about-your-module-with-an-infoyml-file)
[Creating custom modules - Adding a basic controller](https://www.drupal.org/docs/8/creating-custom-modules/adding-a-basic-controller)
[Creating custom modules - Adding a routing file](https://www.drupal.org/docs/8/creating-custom-modules/add-a-routing-file)
[Drupal Console](https://drupalconsole.com/)
[WebProfiler (now part of Devel module)](https://www.drupal.org/project/devel)


* Let’s start our custom module development with a **fresh Drupal installation**.
    * Download and install the **latest stable version of Drupal 8** from Drupal.org
    * Try to resolve any possible issues during the installation process. If you get stucked, call your training mentor.

* With Drupal properly installed and running, let’s create our first custom module for Drupal 8.
    * As a best practice, we should place custom modules separate from contrib ones, so create two directories within the “modules” folder (located in the root of your Drupal installation), named “**contrib**” and “**custom**”.

* Now let’s create our new custom module **inside the custom folder**. Create all the necessary files and settings and name your module “**User Greetings**”, machine name “**user_greetings**”.
    * If you have questions look after the recommended resources in the top of this document!

* Set the **Options** module as a dependency for your custom module.
    * Tip: use its machine name instead of its human-readable name

* Now we will create our first **custom page**:
    * Create the directories “**src/Controller**” inside your module: /user_greetings/src/Controller
    * Create a new Controller class inside that folder called “**WelcomePageController.php**”.
        * Don’t forget to set a proper **namespace** for it!
    * Create a public method “**welcome**”. This method must **return a Response** containing the following message: Welcome to my new Drupal 8 website!
    * Setup a routing for this method, the path should be “**/welcome**”.
    * **Rebuild all caches** (or only the routing cache), visit the created path and see the welcome message.

* Let’s customize the welcome message passing along the username as a wildcard in the URL.
    * **Add a wildcard** in the path of your route. Name it “**username**”.
    * Get the wildcard value in the related method and update the welcome message to look like:<br>
     ```Welcome <username> to my new Drupal 8 website!```
    * Clear the cache again, access “**/welcome/John**” and see the custom welcome message!

* Now go and break your site!!! :o….Yeah, break it!
    * Give another name to your method and see the error.
    * You probably got a blank page,so **activate the debug mode** to see more details about the error.
    * Do not forget to fix the error before keep going.


####Congratulations, you’ve just created a Drupal 8 custom module!!!

---

2.) Services
---

[Symfony - Service Container](http://symfony.com/doc/current/service_container.html)
[Services and dependency injection in Drupal 8](https://www.drupal.org/docs/8/api/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8)
[The Magic Behind Shortcuts Methods is: Services](https://knpuniversity.com/screencast/drupal8-under-the-hood/shortcuts-use-services)
[abstract class ControllerBase](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Controller%21ControllerBase.php/class/ControllerBase/8.2.x)


* Now you should have a **controller** that builds up a welcome message and prints it on the screen. That’s a simple task but a **service should handle it**. Let's create a service to build the welcome message for us!
    * Create a folder named “**Service**” inside your module’s **src** directory: 
    `/user_greetings/src/Service`<br> 
    
    * Inside it create a service class called “**WelcomeMessageGenerator**”.
        * Don’t forget its namespace!
        
    * On that class, create a public method called “**getWelcomeMessage**” that will be responsible for building the welcome message for us. This method can receive the username as a parameter and build the welcome message the same way that our controller was doing: **Welcome <username> to my new Drupal 8 website!**

* Now let’s update our Controller to instantiate our new service class and get the welcome message.
    * Back to “**WelcomePageController**”, update the “**welcome**” method to instantiate the “**WelcomeMessageGenerator**” service class and get the welcome message using the “**getWelcomeMessage**” method passing along the username as an argument.
    
    * Save files and refresh the welcome page, the welcome message should be displayed and work just as before, but now we have a service handling that!

* Let’s improve our service: if a number is passed as the username argument, instead of displaying a weird message we will **sanitize the unwanted value** and display the welcome message without a username.

* Let’s include our service into **Drupal’s Service Container** and make it more performatic and reusable by other modules!
    * To register your new service, create a new services configuration file in the root of your module:
     `/user_greetings/user_greetings.services.yml`
     
    * Inside the configuration file register the “**WelcomeMessageGenerator**” service class with the following nickname:
    `user_greetings.welcome_message_generator`

* OK, service registered, now it’s time to update our Controller and **get our service from the Container**.
    * First thing to do is update “**WelcomePageController**”, make it extend “**ControllerBase**”.
    
    * **Implement/override the “create” method from ControllerBase**. This method must return a new static instance of the “**WelcomeMessageGenerator**” service (use the Service Container to get it). Example:<br>
    `return new static(<welcome_message_generator_service>);`

    * Now create a constructor that receives a  “**WelcomeMessageGenerator**” object as a parameter. Also, create a private property, let’s call it “**welcomeMessageGenerator**” and set it with the object received as argument in the constructor.

    * Finally, update the “**welcome**” method: instead of instantiating your “**WelcomeMessageGenerator**”, just get it using the class property, whose value is the referred service injected in the constructor:
    `$this->welcomeMessageGenerator->getWelcomeMessage()`

    * Save everything, **rebuild caches** and refresh the welcome page. Everything should be running just fine.

####You’ve just used “Dependency Injection”!!!

---
