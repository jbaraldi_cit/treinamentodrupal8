Starting in Symfony2 - Episode 1
================================

Well hallo there! If you're looking for the actual Symfony2 tutorial,
well that's over here: https://github.com/knpuniversity/symfony2-ep1

Inside this repository, you'll find a few great things:

1) The written script of the tutorial in the `knpu/` directory

2) Some files that we use during the screencast in `resources/`.

Since the project is started from nothing, there's nothing else you
need to get going! To see the finished version of the project, visit
the [finish branch](https://github.com/knpuniversity/symfony2-ep1/tree/finish).

And as always, thanks so much for your support and letting us do what
we love!

<3 Your friends at KnpLabs

<br>

---
Exercises
=========

1.) Download and Configure Symfony
---

* This exercise assumes that you have gone through Composer chapter, have it installed on your machine and know how to use it;

* With composer properly installed and globally available on your terminal, let’s use it to scaffold a new Symfony2 Framework project, by typing the following command:

    `composer create-project symfony/framework-standard-edition starwarsevents 2.8`

* The above command will download Symfony2 and all its dependencies into starwarsevents directory; Notice that specifying Symfony’s version is mandatory for this exercise because at the current timing the latest stable version is 3.x and lots of things have changed since 2.x.

* Once everything has been downloaded, enter the starwarsevents directory using the terminal and run the following command to start-up PHP’s built-in webserver through a script that comes bundled with Symfony2:

    `php app/console server:run`

* That will spin up a server in localhost port 8000; Go ahead and try in your webbrowser: http://localhost:8000

* If everything went right you should see a Welcome message in your screen and no error or warning message in the terminal log. If anything weird come up, take some time to investigate and fix those;


---

2.) Create a Twig Template
---

* Let’s use **Symfony’s Console component** to scaffold a new Bundle automatically for us. With your terminal opened and from within the app’s root directory, type in:
        
    `php app/console generate:bundle`

* You will be asked a few questions interactively. For now just answer `Yoda/EventBundle` for Bundle name, `EventBundle` when asked again for the short name, and `yml` (YAML) for the Configuration format; concerning the rest leave the defaults;

* The generator will `scaffold a structure` similar to this:

    ```
    ➜  starwarsevents tree src/Yoda
    src/Yoda
    └── EventBundle
        ├── Controller
        │   └── DefaultController.php
        ├── EventBundle.php
        ├── Resources
        │   ├── config
        │   │   ├── routing.yml
        │   │   └── services.yml
        │   └── views
        │       └── Default
        │           └── index.html.twig
        └── Tests
            └── Controller
            └── DefaultControllerTest.php
    8 directories, 6 files
    ```

* With PHP’s web-server running, if you refresh the homepage you will notice a `“Hello World!”` message. That means everything went just fine! 

* Let’s change it a little bit so the page accepts an **argument from the URL** and prints it out in the screen using a template file;

* In your editor/IDE, open `src/Yoda/EventBundle/Resources/config/routing.yml` , and change the path from `“/”` to `“/{name}”`;

* Now that you added a parameter placeholder to the URL path, go to the related Controller class and in the correct action method, add the parameter as well;

    * The name must be exactly the same as in the route

* At this point your action method already has access to the argument passed to the URL, you just need to pass it along to the templating file; This can be achieved by passing a **second argument to the render()** function, as you can see below:

    `return $this->render('EventBundle:Default:index.html.twig',
      array('name' => $name));`

* Finally in the templating file (the one that ends with .twig extension!), use double curly-braces syntax to print out the injected name variable;

    ```twig
    Hello {{ name }}!
    ```

* To end with this part, just extend the base twig template that comes with Symfony in the `app/Resources/views/base.html.twig` file; 
    * Use Twig’s “extend” keyword
    * Implement some blocks the parent template puts available for us;

* Your `index.html.twig` should end up similar to this:

    ```twig
    {% extends '::base.html.twig' %}

    {% block title 'This is the title' %}

    {% block body%}
        Hello {{ name }}!
    {% endblock %}
    ```

* Ah, you probably learned to use the “router:debug” command from Symfony’s console, right? You should probably know this command is deprecated in version 2.8 and gone from 3.0. The new command is just the opposite and you can already start using it!

    `php app/console debug:router`


---

3.) Insert and Query a Database with Doctrine
---

* **A few things changed** from Symfony’s version used by the class you just had to the version you have installed (probably 2.8.0), so please pay attention to the details on this exercise;

* First of all and before creating a new Doctrine entity you need to **set the correct database configurations** and actually create the database;
    * Go to `app/config/parameters.yml` and set the values according to your system;
    * Choose a **non-existent database** name;
    * In your terminal run `php app/console doctrine:database:create`

* You should have your new **database created** by now! Go ahead and proceed with creating a new Doctrine entity, by issuing the following command:

    `php app/console doctrine:generate:entity`

* For the entity’s name type in **EventBundle:Event**. This will make sure the related entity is put under the EventBundle bundle;

* Create a few fields, **leave the options to defaults**:
    * name, string
    * time, datetime
    * location, string
    * details, text

* At the end, if everything was fine, you should have some **new classes generated for you**, but no database table has been created yet. Tell Doctrine to do that with this:

    `php app/console doctrine:schema:create`

* The related table is finally created; In case you need to make any modifications to that schema, alter the entity’s class (in this case **src/Yoda/EventBundle/Entity/Event.php**) and run:

    `php app/console doctrine:schema:update --force`


---

4.) Generate CRUD Code with Doctrine
---

* First of all, let’s **generate a nice CRUD for the Event** entity type using Symfony’s console; With your terminal opened in the app root directory, type in:

    `php app/console doctrine:generate:crud`

* You will be asked for the **entity name** (don’t forget to **prefix with the bundle name** as well!); Concerning write actions, go for it! Now about the **Configuration format**, in this case **YAML** seems like a good idea, but feel free to choose whatever fits better for you;

* Now open http://localhost:8000/app_dev.php/event/
    * If you changed the **Routes prefix** to something else than **/event** then change accordingly 

* Go ahead and play a little bit. **Add** some events, **delete** others, **edit** some, **list** all but keep a few in the end;


---

5.) Friendly Links and Dates in Twig
---

* Let’s give our site a nice and fresh new layout; First **download** the [Series code](https://drupalize.me/system/files/video/companion_files/symfony2-ep1.tar_.gz) from this chapter in Drupalize.me and **extract** it somewhere.
    * In case the above link doesn’t work, it’s located right below the video related to this exercise;

* First you will copy **new template files** for all Event CRUD pages. From the extracted folder, copy all `*.twig` files under **start/resources/Event/** to the following path relatively to the root of your application:
**src/Yoda/EventBundle/resources/views/Event/**

* Now you need to alter the EventBundle to start **referencing these new templates**. Currently it’s referencing some default templates created by Symfony’s scaffolding tool;
    * Go to the `EventController` and replace all occurrences of “`event/`” inside the `render()` calls to “`EventBundle:Event:`” (don’t use quotes!)

* If you had the curiosity to check these files you noticed they extend a twig file by reading their first line:

    ```twig
    {% extends 'EventBundle::layout.html.twig' %}
    ```

* Go ahead and create this file inside the **views** directory of the **EventBundle**; The only goal here is to show that is possible to create a **hierarchy of template files** by making one extend another; Go ahead and make it extend the base template:

    ```twig
    {% extends '::base.html.twig' %}
    ```

* Before proceeding, let’s fix some issues (yes, bugs :P) left in the new template files;

* If you see in the **EventController** the variables that are passed along to the template files are named **event(s)** and the templates are expecting **entity(ies)**; Edit each template file and replace all occurrences of `entity/entities` to `event/events` respectively;

* This will make the page load again, but some links are still broken, such as **Create new event**; this is because the strings being passed to the **path() twig function** in the template files **do not match the correct routes** names;
    * Edit the route names accordingly, use `Resources/config/routing/event.yml` inside the `EventBundle` as a reference;

* At this point all pages should be loading just fine, but they are still ugly. Let’s finish by applying some stylesheets to them;

* Come back to the extracted folder you downloaded and copy the directory `start/resources/Event/public` to `src/Yoda/EventBundle/resources/public` 

* Remember the base template you just referenced? Let’s edit that one. It’s located in `app/resources/views/base.html.twig`;

* Inside the **stylesheets** block, write the following:

    ```html
    <link rel="stylesheet" href="{{ asset('bundles/event/css/event.css') }}" />
    <link rel="stylesheet" href="{{ asset('bundles/event/css/events.css') }}" />
    <link rel="stylesheet" href="{{ asset('bundles/event/css/main.css') }}" />
    ```

* Finally use Symfony’s console to install the assets in the **web public accessible directory** by running the command:

    `php app/console assets:install --symlink`


---

6.) Create a Fixture for Dummy Data
---

* If you noticed the form to **Create new event** is not functional. It’s missing a **Submit button**. Let’s fix that!

* This form was automatically created by Symfony’s CRUD scaffolding tool, but you can change how it’s built by editing the following file inside the **EventBundle**:
    * `Form/EventType.php`

* Look for the **buildForm()** method. It has a form **builder** which adds elements to construct a form; Go ahead and add a new element in the end of the sequence

    ```php
        use Symfony\Component\Form\Extension\Core\Type\SubmitType;
        ...
        ->add('save', SubmitType::class)
    ```

* Try to refresh the **Create new event** page. Voilá! Now we’re going to use an external **Symfony bundle** to add the **Captcha functionality** to this form, and it’s way simple!

* With your terminal opened, **cd into the root directory** of the application (where composer.json lives) and type in:

    `composer require gregwar/captcha-bundle 2.0.2`

* This is **one of the ways to install** a new dependency via Composer. It already updates both your **composer.json** and **composer.lock** files.

* Register the new installed Bundle in Symfony so the framework loads it in runtime by editing **app/AppKernel.php** and adding the following line to the `$bundles` array:

    ```php
    new Gregwar\CaptchaBundle\GregwarCaptchaBundle(),
    ```

* Now you are ready to add a new element to the form. It’s pretty similar to what you just did to add a submit button! Come back to **buildForm()** method in **Form/EventType.php** add the new Captcha element before the Submit button:

    ```php
    use Gregwar\CaptchaBundle\Type\CaptchaType;
    ...
    ->add('captcha', CaptchaType::class)
    ```

* Refresh and enjoy your fully functional form!! 


---

**OBS:** The imported libraries are throwing errors:

`vendor/gregwar/captcha/CaptchaBuilder.php at line 333` --> `Warning: count(): Parameter must be an array or an object that implements Countable`
