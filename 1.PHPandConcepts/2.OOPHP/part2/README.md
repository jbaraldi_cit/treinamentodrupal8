Object Oriented Ship Battling, Ahem Programming
===============================================

This repository holds the screencast code, script and blueprints for a
secret rebel base for the [Object Oriented Episode 1](https://knpuniversity.com/screencast/oo)
course from KnpUniversity.

Exercises
=========

1.)
---

-Download the kickstart source-code from Drupalize.me and extract it somewhere in your machine. Using the console, access the start directory you just extracted and run PHP’s built-in server on it;
    -Do this even though you did the previous class!

-You just learned that you create a class when you need 1) to hold data (or keep state) or 2) to do some work. The later is what we call by a Service class;

-Inside the lib directory, create a new class BattleManager, which will engage ships in battle. Put it in a file called BattleManager.php;

-Extract the battle() function from the procedural file functions.php into a method on this new class;

-Refactor the code where battle() is called so it instantiates a BattleManager and call the referred method;

-After all the application must keep working as before and you know have encapsulated the battle implementation into a Service class! Congratulations!

:###############################################################################:

2.)
---

-As you can see, there’s still missing a function part of the battling process in functions.php file. Extract didJediDestroyShipUsingTheForce() as a method of BattleManager service class;
    -Adjust its implementation accordingly;
    -Think about making it a private method...

-Let’s keep up with the good work and extract the only procedural function left in functions.php. 

-Create a new Service class called ShipLoader following the same conventions of the previously created classes. Now move the get_ships() method to a new getShips() one from this class;

-Adjust the remaining implementations that made use of get_ships() so they instantiate a ShipLoader object and call the appropriate method instead;

-Again, check the application keeps working fine and Ships can battle indefinitely!

-What about renaming functions.php to bootstrap.php? It should have no code at all despite some require’s...

:###############################################################################:

3.)
---

-Our battle() method from BattleManager service class returns an associative array; What about encapsulate this data structure into a new class too?

-Create a new BattleResult class, with private properties for those three keys from the associative array;

-In this same class, create a constructor method with parameters for setting those 3 properties just created;

-Setters are not necessary for those properties, as they are set during object instantiation; But how do we retrieve their values from outside the class? Go ahead and create Getter methods for all three;

-Update all parts of the application that were expecting an associative array as data structure for a battle result but are now receiving a BattleResult object;

-Make sure battling is still working and the results are presented alright;

:###############################################################################:

4.)
---

-We are going to change the implementation of battle() method from BattleManager service class in order to keep injuries in the ships resulted from the battles;

-Change the method’s implementation so, before checking which was the winner/loser, you set each ship's strength according to each one's health.

    `$ship1->setStrength($ship1Health);`
    `$ship2->setStrength($ship2Health);`

-Debug the state of the ships before and after calling the battle() method. You should see how bad they are after battling each other and how much “life” the winner’s left.
    -Tip: use var_dump() to debug!

:###############################################################################:

5.)
---

-Notice there’s a resources folder in the root of your application folder. If there’s not, you probably forgot to download the source-code from the first exercise. If that’s the case, come back, download and extract it.

-The folder has a file called init_db.php. You need to edit the file and change the first lines with the correct values according to the database server running in your machine.
    -This exercise expects you already have a MySQL-compatible database server running locally

-Then with PHP’s built-in server running, access this file (e.g.: http://localhost/resources/init_db.php). If you see a Ding! message, everything was likely okay. If not, try to solve this by yourself now :-)

-With a MySQL-compatible query tool such as phpMyAdmin or MySQL Workbench, check to see if a database named oo_battle has been created as well as four records on a table called ship;
    -Proceed only if you succeeded until now;

:###############################################################################:

6.)
---

-Notice there’s a resources folder in the root of your application folder. If there’s not, you probably forgot to download the source-code from the first exercise. If that’s the case, come back, download and extract it.

-The folder has a file called init_db.php. You need to edit the file and change the first lines with the correct values according to the database server running in your machine.
    -This exercise expects you already have a MySQL-compatible database server running locally

-Then with PHP’s built-in server running, access this file (e.g.: http://localhost/resources/init_db.php). If you see a Ding! message, everything was likely okay. If not, try to solve this by yourself now :-)

-With a MySQL-compatible query tool such as phpMyAdmin or MySQL Workbench, check to see if a database named oo_battle has been created as well as four records on a table called ship;
    -Proceed only if you succeeded until now;


-Refactor the getShips() method from ShipLoader service class so it connects to the database used in the previous steps and queries for the records in the ship table;
    -Use PDO to connect and query the database!

-You should be able to remove the hardcoded ships from the method once it’s retrieving the data from the database;
    -Your method must still return an array of Ship objects, so make sure to instantiate a new Ship for every record in the database;

-Extract the database connection and query into a new private function within the ShipLoader service class for organization purposes;

-The result will be that the Battleship application will keep working partially. The ship listing will now retrieve the ships from the database; This part must keep working fine. Now battling is broken, we’ll fix it soon...

:###############################################################################:

6.)
---

-So battling is not working right now because we messed up with the Ship key used to populate the select (dropdown) menu values;

-Before going into fixing that, create a new property in your Ship class called $id. Make sure there’s both getter and setter for it, and set its value for all Ships based on the same ID column value coming from the database table; 

-Now update your index.php page to print out each Ship’s ID as the select’s (dropdown) option value;
    -Don’t forget to do this for both dropdowns in the page!

-Once it’s done your page should be submitting the correct ID’s to battle.php page;
    -Optionally you can rename all occurrences of “ship_name” to “ship_id” for a better and meaningful code;

-If you followed everything correctly, you should be getting the IDs of both ships to battle, but in battle.php the application still don’t know which are these Ships;

-You need to create a new public method inside the ShipLoader service class to query for a single Ship once a required ID is passed in as argument;
    -Be creative and do this by your own ;-)

-Refactor the code close to where you actually call BattleManager->battle() so you pass in real Ship objects with their own ID;

-You should have a fully functional application by now. As an extra work if you duplicated the piece of code to convert the data coming from the database into a Ship object, extract into a new private method and make reuse!

:###############################################################################:

7.)
---

-Currently the application is connecting twice to the database. Both are happening from inside the ShipLoader service class;

-Let’s prevent this and reuse the same connection. Extract the lines that connects to the database and returns an instance of the PDO class into a new private method called getPDO().

-Use a new private property in this class to store the PDO instance; 

-Now think a bit and try to use this same property to know whether or not a connection has already been made if someone calls it twice; if it has, return the instance right away, otherwise, make the connection;

(**OBS:** Done in last commit)


:###############################################################################:

8.)
---

-Let’s practise Dependency Injection a bit:

-Edit ShipLoader class and create a constructor method for it, accepting three parameters, each one related to the database required configurations: 
    -Database DSN
    -Database User
    -Database Password

-Create three private properties for these parameters and assign each one of them from inside the constructor;

-Now change the getPDO() method in order to use the class properties instead of the hardcoded values in the connection string; From this point on this class must have no configuration at all anymore! Cool :-)

-As the last part create a configuration associative array in the bootstrap.php file with keys (and values, of course) for those three database parameters;

-As this file is already being required both in index.php and battle.php, this new variable is available to them as well. Make use of it in order to pass in the now required constructor arguments when instantiating the ShipLoader class;

-Your application must keep working fine at the end!


:###############################################################################:

9.)
---

-Let’s create a new Service Container class which will hold our existing services such as PDO, ShipLoader and BattleManager;

-Create the Container class inside the lib directory;

-This class must have private properties for each service it will hold; It also needs to receive a configuration array as a parameter in the constructor method;
    -The configuration will be injected as a dependency

-Create Getter methods for all your services; These methods must create instances of each service class only if they haven’t been created yet; 
    -Store a copy of the service object in the related private property and return it;

-Refactor the code both in index.php and battle.php to start using the Service Container instead of directly instantiating the services;
    -Don’t forget to update your require statements wherever it comes necessary;