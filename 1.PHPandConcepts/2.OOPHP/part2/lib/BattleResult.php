<?php

class BattleResult {
    private $winningShip;
    private $losingShip;
    private $usedJediPowers;

    public function __construct($winningShip, $losingShip, $usedJediPowers){
        $this->winningShip = $winningShip;
        $this->losingShip = $losingShip;
        $this->usedJediPowers = $usedJediPowers;
    }

    
    /**
     * Get the value of winningShip
     */ 
    public function getWinningShip()
    {
        return $this->winningShip;
    }

    /**
     * Get the value of losingShip
     */ 
    public function getLosingShip()
    {
        return $this->losingShip;
    }

    /**
     * Get the value of usedJediPowers
     */ 
    public function getUsedJediPowers()
    {
        return $this->usedJediPowers;
    }
}