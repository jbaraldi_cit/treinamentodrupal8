<?php

class ShipLoader{

    private $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function getShips()
    {
        $shipsArray = $this->fetchShips();

        $ships = array();
        foreach ($shipsArray as $shipData ) {
            $ships[] = $this->createShipFromData($shipData);
        }
        return $ships;
    }

    /**
     * @param int $id
     */
    public function getShipById($id){
        $stmt = $this->pdo->prepare('SELECT * FROM ship WHERE id = :idValue');
        $stmt->execute(array('idValue' => $id));

        $shipData = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->createShipFromData($shipData);
    }

    private function createShipFromData($shipData){
        $ship = new Ship($shipData['name']);

        $ship->setWeaponPower($shipData['weapon_power']);
        $ship->setJediFactor($shipData['jedi_factor']);
        $ship->setStrength($shipData['strength']);
        $ship->setId($shipData['id']);

        return $ship;
    }

    private function fetchShips(){
        $stmt = $this->pdo->prepare('SELECT * FROM ship');
        $stmt->execute();

        $ships = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $ships;
    }

    
}