<?php

namespace Model;

class JediShip extends AbstractShip
{
    use SettableJediFactorTrait;
    
    public function getType()
    {
        return 'Jedi';
    }

    public function isFunctional()
    {
        return true;
    }

    public function getNameAndSpecs($useShortFormat = false)
    {
        $val = parent::getNameAndSpecs($useShortFormat);
        $val .= ' (Jedi)';

        return $val;
    }

}
