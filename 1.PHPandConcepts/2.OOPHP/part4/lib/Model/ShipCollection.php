<?php

namespace Model;

class ShipCollection implements \ArrayAccess, \IteratorAggregate 
{
    /**
     * @var AbstractShip[]
     */
    private $ships;

    public function __construct($ships)
    {
        $this->ships = $ships;
    }

    /**
     * Get the value of ships
     */ 
    public function getShips()
    {
        return $this->ships;
    }

    public function offsetSet($offset, $value)
    {
        $this->ships[$offset] = $value;
    }

    public function offsetGet($offset)
    {
        return $this->ships[$offset];
    }

    public function offsetUnset($offset)
    {
        unset($this->ships[$offset]);
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->ships);
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->ships);
    }

}