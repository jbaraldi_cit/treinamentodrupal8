<?php

namespace Service;

use Psr\Log\LoggerInterface;

class LoggableShipStorage implements ShipStorageInterface
{
    private $shipStorage;

    private $logger;

    public function __construct(ShipStorageInterface $shipStorage, LoggerInterface $logger)
    {
        $this->shipStorage = $shipStorage;
        $this->logger = $logger;
    }

    public function fetchAllShipsData()
    {
        $allShips = $this->shipStorage->fetchAllShipsData();
        $this->log('Fetched all:----------'."\n");
        $this->log(var_export($allShips, true));
        $this->log('======================'."\n");
        return $allShips;
    }

    public function fetchSingleShipData($id)
    {
        $ship = $this->shipStorage->fetchSingleShipData($id);
        $this->log('Fetched single:-------'."\n");
        $this->log(var_export($ship, true));
        $this->log('======================'."\n");
        return $ship;
    }

    private function log($message) {
        $this->logger->info($message);
    }
}
