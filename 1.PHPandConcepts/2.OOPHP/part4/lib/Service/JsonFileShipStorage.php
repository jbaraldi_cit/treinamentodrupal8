<?php

namespace Service;

class JsonFileShipStorage implements ShipStorageInterface
{
    private $filename;

    public function __construct($jsonFilePath)
    {
        $this->filename = $jsonFilePath;
    }

    public function fetchAllShipsData()
    {   
        if (!file_exists($this->filename)){
            throw new \Exception('The provided file does not seem to exist: '.$this->filename);
        }

        $jsonContents = file_get_contents($this->filename);

        return json_decode($jsonContents, true);
    }

    public function fetchSingleShipData($id)
    {
        $ships = $this->fetchAllShipsData();

        foreach ($ships as $ship) {
            if ($ship['id'] == $id) {
                return $ship;
            }
        }

        return null;
    }
}
