<?php

namespace Service;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Container
{
    private $configuration;

    private $pdo;

    private $shipLoader;

    private $battleManager;

    private $shipStorage;

    private $logger;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return \PDO
     */
    public function getPDO()
    {
        if ($this->pdo === null) {
            $this->pdo = new \PDO(
                $this->configuration['db_dsn'],
                $this->configuration['db_user'],
                $this->configuration['db_pass']
            );

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

        return $this->pdo;
    }

    /**
     * @return ShipLoader
     */
    public function getShipLoader()
    {
        if ($this->shipLoader === null) {
            $this->shipLoader = new ShipLoader($this->getShipStorage());
        }

        return $this->shipLoader;
    }

    public function getShipStorage()
    {
        if ($this->shipStorage === null) {
            $this->shipStorage = new PdoShipStorage($this->getPDO());
            //$this->shipStorage = new JsonFileShipStorage(__DIR__.'bla');

            $this->logger = new Logger(LoggableShipStorage::class);
            $this->logger->pushHandler(new StreamHandler(__DIR__.'/../../battle.log', Logger::DEBUG));
            $this->shipStorage = new LoggableShipStorage($this->shipStorage, $this->logger);
        }

        return $this->shipStorage;
    }

    /**
     * @return BattleManager
     */
    public function getBattleManager()
    {
        if ($this->battleManager === null) {
            $this->battleManager = new BattleManager();
        }

        return $this->battleManager;
    }
}
