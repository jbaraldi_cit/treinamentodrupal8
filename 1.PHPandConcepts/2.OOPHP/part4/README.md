Object Oriented Ship Battling, Ahem Programming
===============================================

This repository holds the screencast code, script and blueprints for a
secret rebel base for the [Object Oriented Series](https://knpuniversity.com/screencast/oo)
from KnpUniversity.

Setup
-----

### 1) Database Setup

This project uses a small MySQL database. First, configure your database settings:

A) open `init_db.php` and modify the `$databaseUser` and `$databasePassword` variables.
Make sure the user has permissions to create a database!

B) To create and pre-populate your database, open your favorite terminal application
and run:

```bash
cd /path/to/the/project
php init_db.php
```
That's it! Your database is ready to go!

### 2) Web Server Setup

To get this code working, open your favorite terminal application
and start the built-in web server:

```bash
cd /path/to/the/project
php -S localhost:8000
```

This command will appear to "hang" - but that's perfect! You're
now running a temporary PHP web server (press ctrl+c to stop it
when you're done later).

Pull up the new site by going to:

    http://localhost:8000



Exercises
=========

1.) The Wonder of Class Constants
---

* First of all, download the source-code from Drupalize.me (even though you just came from Part 3), unzip it and go to the start directory. In your terminal, run PHP built-in server and access localhost in your web-browser;

* Let’s add a new feature called Battle Types. Open index.php and scroll down. Right after the ship select boxes, but before the submit button, write down some HTML for a new select box:

```html
... lines 1 - 29
<html>
... lines 31 - 55
    <body>
        <div class="container">
... lines 58 - 92
            <div class="battle-box center-block border">
                <div>
                    <form method="POST" action="/battle.php">
... lines 96 - 119
                        <div class="text-center">
                            <label for="battle_type">Battle Type</label>
                            <select name="battle_type" id="battle_type" class="form-control drp-dwn-width center-block">
                                <option value="normal">Normal</option>
                                <option value="no_jedi">No Jedi Powers</option>
                                <option value="only_jedi">Only Jedi Powers</option>
                            </select>
                        </div>
                        <br/>
                        <button class="btn btn-md btn-danger center-block" type="submit">Engage</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
```

* Refresh and make sure the new element is properly rendered. Here's the idea: each type will cause the BattleManager to battle these two ships in slightly different ways.

* Since the field is named battle_type, open battle.php - the file that handles the submit. Right before calling the battle() method, create a new variable called $battleType set to $\_POST['battle_type']. Then, pass $battleType as a new fifth argument to the battle() method.

* Let's add that new argument! Open BattleManager and find battle(). Give this a new fifth argument: $battleType.

* Writing the battle logic is not the main purpose of this hands-on, so feel free to copy the implementation below (but please make sure you understand it):

``` php
    public function battle(AbstractShip $ship1,$ship1Quantity, AbstractShip $ship2, $ship2Quantity, $battleType)
    {
        $ship1Health = $ship1->getStrength() * $ship1Quantity;
        $ship2Health = $ship2->getStrength() * $ship2Quantity;
        $ship1UsedJediPowers = false;
        $ship2UsedJediPowers = false;
        $i = 0;
        while ($ship1Health > 0 && $ship2Health > 0) {
            // first, see if we have a rare Jedi hero event!
            if ($battleType != 'no_jedi' && $this->didJediDestroyShipUsingTheForce($ship1)) {
                $ship2Health = 0;
                $ship1UsedJediPowers = true;
                break;
            }
            if ($battleType != 'no_jedi' && $this->didJediDestroyShipUsingTheForce($ship2)) {
                $ship1Health = 0;
                $ship2UsedJediPowers = true;
                break;
            }
            // now battle them normally
            if ($battleType != 'only_jedi') {
                $ship1Health = $ship1Health - ($ship2->getWeaponPower() * $ship2Quantity);
                $ship2Health = $ship2Health - ($ship1->getWeaponPower() * $ship1Quantity);
            }
            // prevent 2 non-jedi ships from fighting forever in only_jedi mode
            if ($i == 100) {
                $ship1Health = 0;
                $ship2Health = 0;
            }
            $i++;
        }
        // update the strengths on the ships, so we can show this
        $ship1->setStrength($ship1Health);
        $ship2->setStrength($ship2Health);
        if ($ship1Health <= 0 && $ship2Health <= 0) {
            // they destroyed each other
            $winningShip = null;
            $losingShip = null;
            $usedJediPowers = $ship1UsedJediPowers || $ship2UsedJediPowers;
        } elseif ($ship1Health <= 0) {
            $winningShip = $ship2;
            $losingShip = $ship1;
            $usedJediPowers = $ship2UsedJediPowers;
        } else {
            $winningShip = $ship1;
            $losingShip = $ship2;
            $usedJediPowers = $ship1UsedJediPowers;
        }
        return new BattleResult($usedJediPowers, $winningShip, $losingShip);
    }
```


* Give it a try!. Select one Jedi Starfighter, one CloakShape fighter, and choose "Only Jedi Powers". Hit engage and ... the Jedi Starfighter used its Jedi powers for a stunning victory! If we refresh, one of the ships will use its Jedi powers every single time.

* Now it’s finally time to refactor this code that’s currently using hard-coded strings so it uses class constants instead!

* Add three constants to the BattleManager class:
    + TYPE_NORMAL = normal
    + TYPE_NO_JEDI = no_jedi
    + TYPE_ONLY_JEDI = only_jedi

 * Replace all occurrences of the hard-coded strings to the new constants you just created. Don’t forget to also change them in index.php from within the HTML code!

* If you did it right, everything should still be working fine!


---

2.) Static Methods
---

* In BattleManager class, create a method for retrieving all Battle Types with their descriptions in an array;
    +  getAllBattleTypesWithDescriptions()

* According to what we just learned, this method can be static because it does not change across instances of this class. It’s actually the same because it only returns static properties (constants!). Another sign for that is it doesn’t make use of $this inside the method;

* Now refactor your index.php to make a static call to this new method and instead of printing the battle types one by one in the HTML, iterate over the array and print out them dynamically;

* Remember that from within a class you can make calls to its own static methods and properties by using the self statement instead of the class name? Try that!


---

3.) Namespaces and Core PHP Classes
---

* Namespaces are just awesome, aren’t they? That and auto-loading are just the beginning of modern PHP! ;-) Now that you got the idea of an auto-loader function, copy and paste the following into your bootstrap.php right below the many require statements:

```php
spl_autoload_register(function($className) {
    $path = __DIR__.'/lib/'.str_replace('\\', '/', $className).'.php';
    if (file_exists($path)) {
        require $path;
    }
});
```

* Now let’s do a big refactor! Begin with adding a namespace statement on each of your classes. The actual name of the namespace must comply with the current class directory, according to what the autoloader function is expecting;

* Refresh the page. Your application should be broken! Don’t worry, we’ll fix it soon. Start by adding use statements (right below the namespace) on each file that gives you an error regarding class not found. Once you fixed all the remaining errors, try battling. Fix those too ;-)
You should be able to remove all require statements from bootstrap.php and the app will keep working!!!

* Last missing part! Go to lib/Service/Container.php and change getShipStorage() implementation to use PdoShipStorage() instead of JsonFileShipStorage() (just comment out one like and uncomment the other).

* You’ll notice it will break again. Fix where the errors point out by specifying the correct root namespace for the PDO class which comes built-in with PHP;


---

4.) Composer Autoloading
---

* In your web-browser open up https://getcomposer.org/ and follow the instructions to install Composer according to your Operating System;
  + It’s highly recommended to install Composer globally in your system path. Follow these instructions;

* In the application root directory create a new file composer.json with its contents as below:

```json
{
  "autoload": {
    "psr-4": {
      "": "lib/"
    }
  }
}
```

* As told in the previous lesson, this tells Composer to enable autoloading for your project from within the lib directory;

* Run composer install (or “php composer.phar install” if you didn’t set Composer globally) in your terminal inside the app root dir;
  + Composer will set the proper autoloading files…


* Only one step left! Edit your bootstrap.php and replace the whole custom autoloading function to just the following line:

  `require \__DIR__.'/vendor/autoload.php';`

* Now your app is using Composer’s Autoloader! Everything should be up and running again!


---

5.) Different Exception Classes
---

* Edit lib/Service/Container.php and change getShipStorage() back to use JSON instead of database as its Ship Storage (just comment out one like and uncomment the other).

* Let’s break our application (yes, you read it right! :-P). Still in the Container class, change any part in the .json file path inside getShipStorage() method, so the filename does not match any existing file;

* If you refresh the browser, an ugly error will be thrown to the screen. This is not an exception, but a simple error message. Let’s turn this into a proper Exception!

* Go to JsonFileShipStorage class and alter fetchAllShipsData() so it checks if the argument which should be a valid filepath exists in fact; If not, throw an Exception with a message such as the following:
    + The provided file does not seem to exist: “/path/to/the/file.ext”
    + Remember you don’t need to return the function. The throw statement will stop the method’s execution at that point.

* Now if you refresh the page, instead of a simple runtime error given by file_get_contents() being called in an inexistent file, we have a nice Exception being thrown! And this is catchable, just like a Pokemon! :-P (¬¬’)

* So let’s catch ‘em all! (okay, I stop it here lol) Go to ShipLoader and in getShips() use a try/catch block to catch the exception and instead of interrupting the request, return an empty array of ships. 

* The application should keep working, the page will render but no ships will be available to battle because something wrong happened with the back-end. At least there isn’t an ugly message in the user’s screen. Good job!!


---

6.) ArrayAccess: Treat your Object like an Array
---

* If you followed all steps from the previous exercises your application should be using a JSON file as storage backend for the Ships;

* Let’s change our `BattleResult` class a bit by making it implement the `Serializable` interface. This is a built-in PHP interface which forces the class to have the following methods:
    + serialize()
    + unserialize($data)

* By implementing this interface, you now can control how and which data is going to be serialized for this data (and which will not) when someone calls serialize() on an instance of it; For the sake of this exercise, you can copy and paste the following implementation for those methods:

```php
    public function serialize() {
        return json_encode(serialize([
            $this->usedJediPowers,
            $this->winningShip,
            $this->losingShip
        ]));
    }


    public function unserialize($serialized) {
        list(
            $this->usedJediPowers,
            $this->winningShip,
            $this->losingShip
        ) = unserialize(json_decode($serialized));
    }
```

* Now on `battle.php`, right after `$battleManager->battle` is called, you can try to serialize the referred object and let’s say, write it into a JSON file so another application consumes and upon deserialization, it gets a full representation of the same object and its data/state;

    `file_put_contents("battle_results.json", serialize($battleResult) . "\n", FILE_TEXT | FILE_APPEND);`

* Now that you played a bit with PHP’s built-in interfaces, let’s practise PHP’s magic methods too; But before that, go and change the `ShipStorage` backend back to your MySQL database instead of the JSON one;

* Edit your PdoShipStorage class and add a `__destruct()` method on it; Inside this method, set the $pdo class property to `NULL`; 
    + That way you are force-closing PDO’s connection to the database right when an instance of this class gets destructed (in this scenario, when the script finishes executing);


---

7.) IteratorAggregate: Loop over an Object!?
---

* Right now, in `ShipLoader`, the `getShips()` method returns an array. Instead let’s make it return an instance of `ShipCollection`. This class does not exist yet, go ahead and create it under the Model namespace;

* Create a private property called `$ships` to hold the Ships array. Create also a constructor which will receive the referred array as a parameter and a public Getter method that will retrieve it;

* As you just learned, by implementing PHP’s built-in `ArrayAccess` interface, you can give array-like capabilities to an object; Go ahead an do it! Don’t forget to implement all methods this interface obligates;

* At this time, you will be able to add a new Ship using array syntax to what is now an instance of `ShipCollection`; 
    + Check line 10 of your `index.php`! 

* However you are still unable to loop/iterate through this array, ‘cause a special power is still missing; Go ahead and make `ShipCollection` implement `IteratorAggregate` interface;
    
* As the purpose of this exercise is to learn how to implement multiple interfaces in a single class, you can just copy and paste the following implementation required by the IteratorAggregate interface:

    ```php
    public function getIterator()
    {
        return new \ArrayIterator($this->ships);
    }
    ```

* You should be all set by now! Although ShipCollection is an object you can also treat it like an array!


---

8.) Traits: "Horizontal" Reuse
---

* Let’s create a new type of Ship called `JediShip`. In the model directory and namespace, add this new class;

* Make it extend the `AbstractShip` class and implement the required methods. 

* Both `getJediFactor()` and `setJediFactor($jediFactor)` should act upon a private property in this class; Add this property so this Getter and Setter work properly;

* By now this new `JediShip` class should be ready to be instantiated. Go ahead and add it directly in your index.php right below where a `BrokenShip` is being created; 

* Now if you look at both `Ship` and `JediShip` classes you’ll notice both share a few things in common, such as the `$jediFactor` property and its Getter and Setter;
    + Say no to code duplication!

* To solve this you could create a new class and use inheritance, but for this specific use case that’s not the best deal; Instead, let’s make sure a of trait to make reuse of these parts;

* Create a new file called `SettableJediFactorTrait.php` inside the Model directory and namespace;

* Add the following trait implementation to it:

    ```php
    trait SettableJediFactorTrait
    {
        private $jediFactor;
        public function getJediFactor()
        {
            return $this->jediFactor;
        }
        public function setJediFactor($jediFactor)
        {
            $this->jediFactor = $jediFactor;
        }
    }
    ```

* Now go back to both `Ship` and `JediShip` classes, remove these methods and property and in the beginning of the class implementation add the following line that will import the trait’s contents:

    ```php
    use SettableJediFactorTrait;
    ```

* At this point only battling with `JediShip` will still not work. The rest should be working smoothly.


---

9.) Object Composition FTW!
---

* This last exercise illustrates how it’s possible to extend a certain class without using inheritance, which is not desirables (or does not make sense) in some circumstances. What we show here is called Object Composition;

* Create a new class `LoggableShipStorage` that implements `ShipStorageInterface`. It must live inside the Service directory and namespace;

* As it’s told by the related interface, you must implement the following methods:
    + `fetchAllShipsData()`
    + `fetchSingleShipData($id)`

* `LoggableShipStorage` will not actually do any of the ship-loading work - it'll offload all that hard work to `PDOShipStorage`. 

* To do that, add a constructor to this class that accepts an argument. This argument can be any object that implements `ShipStorageInterface`; Store this into a private property into the class;
    + Yes, you are doing Dependency Injection! ;-)

* Now for those two methods required by the interface, defer their processing and return to the injected object;

* At last, go to your `Container` class and inside `getShipStorage()` you can now use your `LoggableShipStorage` class injecting the `PDOShipStorage` object that you have. 

* This allows you to act before and after fetching Ships and any other actions the inner class would do; Nice, huh!?


---


Composer
===

The Wonderful World of Composer
---

* This exercise assumes that you have gone through Object Oriented PHP Part 4. Chapter 8 of that course shows how to install Composer according to your system. Also the implementation contained here is based on the same application developed there;

* With composer properly installed and globally available on your console, let’s add a very famous Logging mechanism to our application named Monolog;

* There are basically two ways to perform this action:
    + Manually edit composer.json in the root directory of the app, add the dependency in the JSON and run composer install;
    + OR run composer require monolog/monolog

* Either way you will end-up with a composer.json file similar to this:
    
    ```json
    {
        "autoload": {
            "psr-4": {
            "": "lib/"
            }
        },
        "require": {
            "monolog/monolog": "^1.21"
        }
    }
    ```

* Composer already downloaded Monolog and its dependencies into the vendor directory located in the app root; As you are already using its class autoloader, you can start using the new provided mechanism!

* Let’s start by adding a new argument to the constructor of the `LoggableShipStorage` class. This will allow us to inject an instance of a Logger into the class; 

* Type hint the argument with `LoggerInterface`. This interface is standardized in a PSR. This means that not only Monolog logger is acceptable but any other logging mechanism that is according to that interface;
    + You can **“use Psr\Log\LoggerInterface”**;
    + Store this into a private property of this class;

* Now that you made possible to inject a logger, let’s go to the Container class where the LoggableShipStorage class is being instantiated; 

* Create a new private property to hold the logger object; From inside `getShipStorage()`, right before instantiating `LoggableShipStorage`, add the following lines of code:

    ```php
    $this->logger = new Logger(LoggableShipStorage::class);
    
    $this->logger->pushHandler(new StreamHandler(__DIR__.'/../../battle.log', Logger::DEBUG));
    ```

* This will create a new Logger instance, store it on the private $logger property (assuming you named like this) and set a handler to save the log entries into a text file;

* Don’t forget the use statements for Monolog classes:

    + `use Monolog\Logger;`
    + `use Monolog\Handler\StreamHandler;`

* At last, inject this logger object as a dependency in the second argument of `LoggableShipStorage`;

* Finally use your brand new logging mechanism to record entries in the file with something like this in `LoggableShipStorage`:

    ```php
    private function log($message) {
    $this->logger->info($message);
    }
    ```

* Now each time you refresh the page, log entries will be recorded in a file called battle.log in the root directory of the application; Go check it out!


---