Object Oriented Ship Battling, Ahem Programming
===============================================

This repository holds the screencast code, script and blueprints for a
secret rebel base for the [Object Oriented Episode 1](https://knpuniversity.com/screencast/oo)
course from KnpUniversity.

Exercises
=========

1.) Override an Inherited Method 
---

* Download the kickstart source-code from Drupalize.me and extract it somewhere in your machine. Using the console, access the start directory you just extracted and run PHP’s built-in server on it;
    + Do this even though you did the previous class!

* Run the init_db.php script under the resources folder. It will recreate the table used in the previous classes, but with an additional column;

* Now that you’ve learned about Class Inheritance, create a new Model class called RebelShip, which extends from Ship;
    + Don’t forget to require it in your bootstrap!

* Go to the ShipLoader service class. Change the method createShipFromData() so it considers the team field coming from the database in order to instantiate objects of type Ship or RebelShip depending on the value;

* Now edit the Ship class and create a new type property;
    + Create Getter method for it;
    + Make it return a string such as “Empire”;

* To finish override the getType() method of RebelShip so instead of returning the string from its parent class (inherited method), it returns the string “Rebel”;

* Test everything you did by creating a new column in the table of ships that’s presented on the screen; this column must show each ship’s type, whether it’s from the Empire or it’s Rebel;


:###############################################################################:

2.) Introduction to Abstract Classes
---

* In the RebelShip class, override the getNameAndSpecs() method so it returns what the parent method does and concatenates a “ (Rebel)” string in front of it;

* Now create an abstract class for holding only common properties and method implementations among Ship and RebelShip model classes;
    + You can call it AbstractShip

* Change Ship and RebelShip so both extend from AbstractShip and remember to remove any methods and/or properties that will be inherited “as-is”;
    + No need to override what’s not going to change :P

* As both classes are now extending from AbstractShip, check if it’s necessary to call any parent method to keep functionality working as before; 

* Also test the whole application (page opening and battle engaging) to see if any further adjustments are needed;
    + You may need to update any type-hinting from Ship to AbstractShip on your code!


:###############################################################################:

3.) Introduction to Interfaces
---

* The goal here is to give flexibility to ShipLoader so it can load ship data from the database or a JSON file;

* Create new class PdoShipStorage into Services directory. Add 2 methods to this class: fetchAllShipsData() and fetchSingleShipData($id);

* Move all querying logic from ShipLoader to the PdoShipLoader class;

* PdoShipStorage class needs to access the PDO object, use dependency injection to solve it.

* The first challenge is to fix all things to keep the app working properly. 

* Copy JsonFileShipStorage.php from resources folder into your Services directory, and require it in the bootstrap.php;

* The challenge now is to create an interface that will be implemented by both JsonFileShipStorage and PdoShipStorage;

* Once you’ve done that, remember to change any type-hinting from PdoShipStorage to the new interface you just created; 

* Finally you should be able to go into your Service Container and in the getShipStorage() method switch from PdoShipStorage to JsonFileShipStorage;
    + Pass in the path to resources/ships.json to the class constructor;

* If the app continues working properly retrieving data from the JSON file instead of the database, congrats!!!
