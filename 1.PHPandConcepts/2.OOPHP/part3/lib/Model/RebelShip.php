<?php

class RebelShip extends AbstractShip 
{
    public function getType()
    {
        return 'Rebel';
    }

    public function getNameAndSpecs($useShortFormat = false)
    {
        return parent::getNameAndSpecs($useShortFormat).' (Rebel)';
    }
}
