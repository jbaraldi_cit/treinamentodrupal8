<?php

class Ship {
    private $name;
    private $weaponPower;
    private $jediFactor;
    private $strength;

    public function __construct(string $name){
        $this->name = $name;
    }

    public function getNameAndSpecs($format = 'long'){
        if (strcmp($format, 'long') == 0) {
            printf(
                nl2br("Name: %s\nWP: %s \n JF: %s \n Str: %s \n "),
                $this->name, $this->weaponPower, $this->jediFactor, $this->strength
            );
        } elseif (strcmp($format, 'short') == 0) {
            printf(
                nl2br("%s/%s/%s/%s"),
                $this->name, $this->weaponPower, $this->jediFactor, $this->strength
            );
        } else {
            throw new Exception('Tipo de impressao invalido');
        }
    }

    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getWeaponPower()
    {
        return $this->weaponPower;
    }


    public function setWeaponPower($wp)
    {
        $this->weaponPower = $wp;

        return $this;
    }

    public function getJediFactor()
    {
        return $this->jediFactor;
    }


    public function setJediFactor($jediFactor)
    {
        $this->jediFactor = $jediFactor;

        return $this;
    }

    public function getStrength()
    {
        return $this->strength;
    }

    public function setStrength($strength)
    {
        $this->strength = $strength;

        return $this;
    }
}
