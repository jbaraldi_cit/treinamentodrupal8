Object Oriented Ship Battling, Ahem Programming
===============================================

This repository holds the screencast code, script and blueprints for a
secret rebel base for the Object Oriented Episode 1 course from KnpUniversity.

Exercises
=========

1.)
---
-Download the kickstart source-code from Drupalize.me and extract it somewhere in your machine. Using the console, access the start directory you just extracted and run PHP’s built-in server on it;

-Using a web browser access your running PHP’s built-in web server (e.g.: http://localhost:8080) and verify the Battleship application is running properly;

-Now using a text-editor such as Atom let’s create a new file in the root of this application. Name it play.php.

-Create a new empty PHP Class that represents a Ship; right after declaring this class, instantiate it as a PHP Object;

-Access this new play.php file directly in your browser (e.g.: http://localhost:8080/play.php) and while you must see nothing, no errors should show up as well;
    -This probably means it’s working! ;-)

-Add a name property in this class, set a value for it after an object of this class is instantiated and print it out in the page;

:###############################################################################:

2.)
---
-Still in the Ship class, create some new properties such as weapon power, jedi factor and strength;

-Set some values for these new properties after instantiating the class; 

-Create a new method called getNameAndSpecs() which must return a string consolidating the Ship’s name and values of its properties;
    -Print it out in the page for testing purposes!

-Change the signature for the getNameAndSpecs() method so it accepts an argument. Name it $format;

-Now refactor the method’s implementation so if $format is ‘long’ you return a string with labels for the properties. If $format is ‘short’, return a pretty straight-forward string instead.

-Add a name property in this class, set a value for it after an object of this class is instantiated and print it out in the page;

:###############################################################################:

3.)
---
-Let’s bring the Ship class we just created to our real Battleship application!

-First of all, create a lib directory in the app root and move the Ship class to a single file called Ship.php;
    -To make sure everything is still working, come back to play.php and require the Ship class from the external file. Refresh this page and see if it’s okay;

-Now you will refactor the application to use your new class instead of raw PHP arrays;

-Edit file functions.php and change the get_ships() function so instead of returning an array of arrays, it returns an array of Ship objects;
    -Don’t forget to require the Ship class in the file!

-Come back to your browser and access the index.php page. You’ll notice some errors on the screen because now you’re using Ship objects but the page still expects simple arrays;

-Fix all apparent errors and warnings so the page is back to normal. Don’t engage the Ship in battle yet! ;P

:###############################################################################:

4.)
---

-Edit your Ship class and change the visibility modifier of all its properties from public to private;

-Now to still be able to store and retrieve values from/to these properties, create Setter and Getter methods for them; 
    -If you are not familiar with these, look out for Mutator methods in the Resources section below;

-Refactor the code from index.php and get_ships() so they start using the new methods for storing and getting the Ship properties values;

-The page must keep working fine. Again, don’t expose your Ships to battle as they’re not ready yet...

:###############################################################################:

5.)
---

-Now your Ships will finally battle each other! But first, let’s refactor some code so this is possible;

-When you click Engage after selecting both Ships in the page, the browser will submit a HTTP POST request to the battle.php page;

-Notice this file makes a call to a battle() function which is therefore declared in functions.php. Go to its implementation and change it accordingly so instead of Ship arrays it accepts Ship objects as arguments;
    -Tip: make use of Type Hinting!

-Now make sure all access to Ship properties are done through Getters and Setters, both in functions.php and battle.php;

-In the end you should have a fully functional Battleship application! Enjoy :D
