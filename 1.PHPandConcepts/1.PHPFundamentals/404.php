<?php
    include_once 'layout/header.php';

    http_response_code(404);?>
    <div class="jumbotron">
        <div class="container">
            <h1><?php echo strtoupper('404 page not found! sorry... :\'('); ?></h1>
        </div>
    </div>
    
<?php
    include_once 'layout/footer.php';
?>