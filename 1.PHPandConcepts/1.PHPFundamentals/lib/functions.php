<?php
    function pets_count(){ return count(get_pets()); }

    function get_connecion(){
        $sqlConfig = include 'lib/config.php';

        static $pdo;

        if(!isset($pdo)){
            $pdo = new PDO($sqlConfig['db_dsn'], $sqlConfig['db_user'], $sqlConfig['db_pass']);
        }

        return $pdo;
    }

    /* OLD VERSION */
    /*function get_pets(){
        $strjson = file_get_contents('resources/pets.json'); 
        return json_decode($strjson, true);
    }*/

    /** SQL version */
    function get_pets(){
        $pdo = get_connecion();
        
        $result = $pdo->query('SELECT * FROM pet');
        return($result->fetchAll());
    }

    function save_pet(array $pet){
        $pets = get_pets();
        $pets[] = $pet;
        $json = json_encode($pets, JSON_PRETTY_PRINT);
        file_put_contents('resources/pets.json', $json);
    }

    function get_pet ($id){
        $pdo = get_connecion();
        
        $query = 'SELECT * FROM pet WHERE id = :idVal';
        $stmt = $pdo->prepare($query);
        $stmt->bindParam('idVal', $id);
        $stmt->execute();

        return $stmt->fetch();
    }