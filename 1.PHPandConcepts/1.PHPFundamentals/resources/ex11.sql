--create db
CREATE DATABASE air_pup;

--crate new table pets
USE air_pup;

CREATE TABLE pets(
    -> id int(11) AUTO_INCREMENT,
    -> name varchar(255),
    -> breed varchar(100),
    -> PRIMARY KEY (id)
    -> ) ENGINE=InnoDB;

--describe command
DESCRIBE pets;

--insert dummy rows
INSERT INTO pets(name, breed) VALUES ("pet1", "dog1");
INSERT INTO pets(name, breed) VALUES ("pet2", "dog2");
INSERT INTO pets(name, breed) VALUES ("pet3", "cat1");

--all rows, just the name column
SELECT name FROM pets;

--only pet w/ id 2
SELECT * FROM pets WHERE id=2;