--delete some row
DELETE FROM pet WHERE id = 3;

--output from phpMyAdmin for table alteration
ALTER TABLE `pet` ADD `age` VARCHAR(255) NOT NULL AFTER `breed`, ADD `weight` INT(4) NOT NULL AFTER `age`, ADD `bio` TEXT NOT NULL AFTER `weight`, ADD `filename` VARCHAR(255) NOT NULL AFTER `bio`;