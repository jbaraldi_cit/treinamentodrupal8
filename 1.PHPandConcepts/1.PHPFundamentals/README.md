Epic PHP!
=========

This repository holds the screencast code, script and coding activities for
episode 1 of the upcoming PHP series "Writing Epic PHP in one month".

Contributing
------------

Progress will be posted to this repository as we work. If you'd like to contribute,
either open up an issue or (even better) fork the code and create a pull
request. We want to create the best PHP tutorial for beginners, a job that's
best solved by as many people as possible :).

Exercises
---------

1.) 
-Transform all characters of the Hello World text to uppercase.

:###############################################################################:

2.) 
-Create an array with three elements, each one containing a string with “Block 1”, “Block 2” and so on.

-Replace the three titles of the Home Callouts with the contents of this array.

:###############################################################################:

3.)
-Create an associative array of Pets with keys:
    -name
    -breed
    -weight
    -image_file
        -the image files are in the folder with same name!

-Loop through this array and print its contents as HTML in the page

:###############################################################################:

4.)
-Read the JSON file located into the “resources” folder.

-Print out a heading title with the amount of pets in the JSON file;
    -e.g.: There are X animals in the database!

-Print out the contents of the parsed JSON as HTML blocks;

:###############################################################################:

5.)
-Manually edit the JSON file used in the previous exercise and include your own puppy! ;-)
    -Do not set any filename property for it. Handle this gracefully in your PHP code (do not show any image at all).
    -On another pet, keep the filename key (In the JSON file) but leave it as an empty string;
        -also don’t show any image;

-Check if the pet’s weight is greater or equal to 8. 
    -If yes, print “I need some exercise.”, otherwise, print ”I’m slim!”
    -Make use of a Ternary Operator If Statement;

:###############################################################################:

6.)
-Inside index.php, create a PHP function that returns the amount of pets in the JSON file.

-Create a new PHP file called “reports.php” and print out “The database already has X pets”, where X is the amount of pets from the function just created;
    -how is it possible to call the function from within reports.php?

-Add a link to this new Reports page to the header’s menu bar;

-Create a new file called “functions.php” inside a “lib” directory (create it) and move the function written in step 1 to it;

-Now make sure your report.php is still working!

:###############################################################################:

7.)
-Create a directory “layout” in the root of your site.

-Inside that directory, create two new PHP files:
    -header.php
    -footer.php

-Edit your index.php and cut the following parts:
    -everything from the beginning of the document until the end of the site header, including the main horizontal menu bar;
    -the page’s footer, including the closing tags for <body> and <html>

-Paste them into the files created in step 2, respectively.

-Now require those files in index.php accordingly so the page looks exactly the same as before.

-Add a header and footer to the reports.php page you created on the previous exercise.

-On each page make only the related link (currently accessed) with the “active” class and style;

:###############################################################################:

8.)
-Create a new PHP file called “add_pet.php” under the root of your site;

-Edit the site’s header and add a link to the homepage and another to this new page you just created;

-Create a HTML form, with fields for each of the pet properties stored on the JSON file used on the exercises of the previous chapter;
    -make sure your form is submitting its values into the same page! (how to do that?)
    -make sure the form’s method is POST

-Using Chrome Dev Tools (or Firebug), inspect the form submission and check if the values are being sent along with the HTTP request the browser does;

-CHALLENGE: use a file field for the image upload. How to get the actual file (and not only its filename) in the server side?

:###############################################################################:

9.)
-Edit your add_pet.php file and debug (var_dump) the contents of the $_POST super global variable only if the form has been submitted;

-Store the contents of each submitted field in a separate variable;
    -Extra: do it first in the regular way, then try to use extract() PHP built-in function;

-Store the new Pet in the existing JSON file;
    -keep all existing pets in the file as well;
    -find out how to store a formatted (pretty) JSON into the file, for better readability

:###############################################################################:

10.)
-After recording the new pet (submit the form), redirect the user to the homepage;

-Extract the lines that actually save a new pet into a new separate function (in functions.php file). This function should receive an array as argument and persist it into the JSON file.

-Create a new file called 404.php in the root of your project;

-When this page is accessed through the browser, your PHP code must send a HTTP Status Code of 404 Not Found.
    -use Chrome Dev Tools Network tab to make sure it’s working!

:###############################################################################:

11.)
-Connect to your local MySQL server using the command-line mysql client;

-Create a new database schema called air_pup;

-Within this schema, create a new table called pets, with the following fields and respective types:
    -id: integer, auto increment, primary key
    -name: varchar (50)
    -breed: varchar (50)

-Use mysql describe command to check if you table structure is according to the expected;

-Insert some dummy rows to this table;

-Select all rows but only the name column;

-Select only the pet with ID = 2;

:###############################################################################:

12.)
-Delete some row from the table created on the previous exercise, using MySQL command-line client;
    -Remember to use the WHERE clause to prevent deleting all records from the table!

-Open up MySQL Workbench and create a new connection to your local MySQL server;

-Using MySQL Workbench, create additional columns on your pets table;
    -The table must reflect all fields containing in your HTML form for creating a new pet;

:###############################################################################:

13.)
-Create a new database connection to the MySQL database running locally on your machine;
    -Use PDO to do this!

-Query all pets from the database.
    -You should have some dummy records as we created in the previous exercise;

-Refactor your getPets() implementation in order to retrieve the pets from the database table instead of the JSON file;
    -The Homepage and Reports pages on your website should keep working as the same!

:###############################################################################:

14.)
-Edit the function where you’re connecting to the local MySQL database from your application using PHP Data Objects (PDO);

-Extract the currently hard-coded database connection parameters from the PDO class instantiation to a new file called config.php, inside the “libs” directory;
    -Make sure it’s required where necessary!

-Change the connection string to use the parameters from the config.php file you just created;

:###############################################################################:

15.)
-Create a new page called pet.php in the root of your project;
    -This page will show data for only one pet at a time;

-This page will retrieve the ID of a certain pet through the query string sent along with the request by the browser;
    -This ID will be used to query the correct pet to show;

-This new page must have the site’s header and footer, and must show all info from the pet whose id is on the URL in the parameter called pid;

-In the homepage and for each pet printed out on the screen, also print a link to the pet.php page passing along the ID of the referred pet as a query string in the URL.
    -Validate this ID so in case it does not match a pet in the database, you redirect the user to the 404.php page you created previously;

:###############################################################################:

16.)
-Refactor your code so the database connection is made only once in a single function. This function then can be called anywhere when a connection is necessary;

-The connection must be made only once no matter how many calls you make to the function that does it;
    -Tip: learn how to use static variables;

-Test your pid query string parameter against a SQL Injection vulnerability attack;
    -If vulnerable, fix it using PDO Prepared Statements;

:###############################################################################:
:###############################################################################: