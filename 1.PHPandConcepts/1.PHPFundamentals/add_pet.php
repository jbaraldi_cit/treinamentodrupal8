<?php 
    include_once 'layout/header.php';

    $pet = [];
    if(isset($_POST['name'])){
        extract($_POST);
        $pet = [
            'name' => $name,
            'breed' => $breed,
            'age' => $age,
            'weight' => $weight,
            'bio' => $bio
        ];
        
        if(isset($_FILES['userfile']['error']) && ($_FILES['userfile']['error'] == UPLOAD_ERR_OK)){
            $pet['filename'] = $pet['name'].'-'.$_FILES['userfile']['name'];
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'images/'.$pet['filename']);
        }

        save_pet($pet);
        
        header('Location: /');
    } else {
        $pet['name'] = '';
    }
?>

    <div class="jumbotron">
        <div class="container">
            <h1>Add your pet!</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <form enctype="multipart/form-data" action="add_pet.php" method="POST">
                    <div class="form-group">
                        <label for="pet-name" class="control-label"> Pet Name </label>
                        <input type="text" name="name" id="pet-name" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="pet-breed" class="control-label"> Breed </label>
                        <input type="text" name="breed" id="pet-breed" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="pet-age" class="control-label"> Age </label>
                        <input type="text" name="age" id="pet-age" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="pet-weight" class="control-label"> Weight (lbs) </label>
                        <input type="number" name="weight" id="pet-weight" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="pet-bio" class="control-label"> Bio </label>
                        <textarea name="bio" id="pet-bio" class="form-control" /></textarea>
                    </div>

                    <div class="form-group">
                        <!-- Trecho retirado de php.net -->
                        <!-- MAX_FILE_SIZE deve preceder o campo input -->
                        <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
                        <!-- O Nome do elemento input determina o nome da array $_FILES -->
                        Send your pet's pic (300KB tops): <input name="userfile" type="file" accept="image/*" />
                    </div>

                    <button type="submit" class="btn btn-primary">
                        <span class="glyphicon glyphicon-heart"></span>
                        ADD PET!
                        <span class="glyphicon glyphicon-heart"></span>
                    </button>
                </form>
            </div>        
        </div>
    </div>

<?php include_once 'layout/footer.php'; ?>