<?php 
    include_once 'layout/header.php'; 
    include_once 'lib/functions.php';

    $pets = get_pets();
?>

    <div class="jumbotron">
        <div class="container">
            <h1><?php echo strtoupper('Hello, world!'); ?></h1>

            <p>There are <?php echo pets_count(); ?> animals in the database!</p>

            <p><a class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <?php
                foreach ($pets as $pet){?>
                    <div class="col-md-4 pet-list-item">
                        <h2> 
                            <?php echo $pet['name']; ?> 
                        </h2>

                        <?php if(array_key_exists('filename', $pet) && isset($pet['filename']) && !empty($pet['filename'])) { ?>
                            <img src="images/<?php echo $pet['filename']; ?>" class="img-rounded"; />
                        <?php } else {?>
                            <br>
                            <?php echo strtoupper('image not avaible!');?>
                            <br><br>
                        <?php } ?>
                        
                        <blockquote class="pet-details">
                            <span class="label label-info"> <?php echo $pet['breed']; ?> </span> <br>
                            <?php echo $pet['age']; ?>
                            <?php echo $pet['weight']; ?> lbs 
                            <br>
                            <?php $exercise = ($pet['weight'] > 8 ? 'I need some exercise.' : 'I’m slim!');
                            echo $exercise; ?>
                        </blockquote>

                        <?php echo $pet['bio']; ?>
                        
                        <p><br>
                        <a class="btn btn-default" href="/pet.php?pid=<?php echo $pet['id']; ?>">
                            View details &raquo;
                        </a></p>
                    </div>
                <?php } ?>
        </div>
    </div>

<?php include_once 'layout/footer.php'; ?>